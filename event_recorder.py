import sys
import argparse
import time
import pymongo
import threading
import traceback
import logging
import json
from lib.octopyapp.octopyapp import OctopyApp


APP_ID = "Event Recorder"
LOG_LEVEL = logging.DEBUG

WRITE_INTERVAL = 10
PRUNE_INTERVAL = 60

DEFAULT_TOPICS_LIST = [ '#' ]
DEFAULT_RETENTION_TIME = 86400  # 1 day

DATABASE_NAME = 'Ocotopy'
EVENT_COLLECTION_NAME = 'events'

# FIXME: Retained messages should not be purged automatically.
#        Possibly handle structure messages specially, keeping them separate.


class EventRecorder(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.buffer = []
        self.db_client = None
        self.buffer_lock = threading.Lock()
        self.subscribe(self.topics['recorder']['query'], self.on_mqtt_query_message)
        for topic in self.config['recorder']['topics']:
            self.subscribe(topic)


    def verify_config(self):
        super().verify_config()
        if not 'recorder' in self.config:
            self.config['recorder'] = {}
        if not 'topics' in self.config['recorder']:
            self.config['recorder']['topics'] = []
        if not isinstance(self.config['recorder']['topics'], list):
            raise TypeError(f"Config key 'recorder'>'topics' must be a list")
        if len(self.config['recorder']['topics']) == 0:
            self.config['recorder']['topics'] = DEFAULT_TOPICS_LIST
        if not 'retention time' in self.config:
            self.config['retention time'] = DEFAULT_RETENTION_TIME
        return self.config


    def start(self):
        super().start(start_loop = False)
        # Connect to database
        # TODO: Make sure it succeeds
        self.connect_to_database()
        # Wait for MQTT connection
        logging.info("Waiting for MQTT connection")
        while not self.connected:
            self.mqtt_client.loop(0.1)
            self.mqtt_client.loop_misc()
        tick = 0.1  # 10 Hz refresh rate
        last_write = time.time()
        last_prune = time.time()
        last_mqtt_misc = time.time()
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                if current_time > last_write + WRITE_INTERVAL:
                    # Write out buffer to database
                    last_write = current_time
                    self.write_buffer_to_database()
                if current_time > last_prune + PRUNE_INTERVAL:
                    # Write out buffer to database
                    last_prune = current_time
                    self.prune_database()
                if current_time > last_mqtt_misc + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc = current_time
                    self.mqtt_client.loop_misc()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        logging.info('Exiting: ' + reason)
        self.mqtt_client.loop()
        self.stop()
        if self.buffer_lock.locked():
            self.buffer_lock.release()
        self.write_buffer_to_database()
        self.prune_database()
        self.db_client.close()
        sys.exit(0)


    def connect_to_database(self):
        logging.info("Connecting to database")
        mongodb_host = self.config['recorder']['mongodb']['host']
        mongodb_port = self.config['recorder']['mongodb']['port']
        mongodb_url = f"mongodb://{mongodb_host}:{mongodb_port}/"
        self.db_client = pymongo.MongoClient(mongodb_url)
        self.db = self.db_client[DATABASE_NAME]
        self.db_collection = self.db[EVENT_COLLECTION_NAME]
        logging.info("Connected to database")


    def on_message(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.warning(f"Payload on topic '{msg.topic}' was not JSON parsable")
            return
            payload = msg.payload
        data = {
            "topic": msg.topic,
            "payload": payload,
            "qos": msg.qos,
            "retain": msg.retain,
            "timestamp": time.time()
        }
        self.buffer_lock.acquire()
        self.buffer.append(data)
        self.buffer_lock.release()


    def on_mqtt_query_message(self, client, userdata, msg):
        query_start = time.time()
        logging.debug(f"New query: {msg.payload}")
        try:
            payload = json.loads(msg.payload)
        except json.decoder.JSONDecodeError as err:
            logging.error(f"Error parsing query: {err}")
            return
        if not isinstance(payload, dict):
            logging.error(f"Invalid data type for query. Expected dict. Got {payload.__class__.__name__}.")
            return
        if 'query' in payload:
            query = payload['query']
        elif 'topic' in payload:
            query = { 'topic': payload['topic'] }
            if 'timestamp' in payload:
                query['timestamp'] = payload['timestamp']
        res_cursor = self.db_collection.find(query)
        res_list = [ { 'topic': event['topic'], 'payload': event['payload'] } for event in res_cursor ]
        return_payload = json.dumps(res_list)
        if 'return topic' in payload:
            return_topic = payload['return topic']
        else:
            return_topic = self.topics['recorder']['default return']
        self.publish(return_topic, return_payload)
        query_time = time.time() - query_start
        logging.debug(f"Query finished in {query_time} seconds and produced {len(res_list)} events(s)")


    def write_buffer_to_database(self):
        logging.info(f"Writing {len(self.buffer)} items to storage")
        self.buffer_lock.acquire()
        if (len(self.buffer) > 0):
            res = self.db_collection.insert_many(self.buffer)
            self.buffer.clear()
        self.buffer_lock.release()


    def prune_database(self):
        timelimit = time.time() - self.config['recorder']['retention time']
        logging.info(f"Pruning database")
        query = { "timestamp": { "$lt": timelimit } }
        doc = self.db_collection.delete_many(query)




if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    er = EventRecorder(config)
    er.start()
